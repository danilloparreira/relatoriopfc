public abstract interface RESTController <T extends AbstractEntity,
					DAO extends DAOGeneric<T>> {
	public ResponseEntity<T> find(Long id);
	public ResponseEntity<List<T>> findAll();
	public ResponseEntity<Void> save(T entity,
			UriComponentsBuilder ucBuilder);
	public ResponseEntity<T> merge(Long id, T entity);
	public ResponseEntity<T> delete(Long id);
}