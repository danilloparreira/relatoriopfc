public class Main {
  public static void main(String[] a) throws Exception {

    EntityManagerFactory emf = Persistence.
		createEntityManagerFactory("PERSISTENCE_UNIT");
    EntityManager em = emf.createEntityManager();
    em.getTransaction().begin();
    // Executa uma transa��o
    em.getTransaction().commit();
    em.close();
    emf.close();
  }
}
