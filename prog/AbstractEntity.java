@MappedSuperclass
public class AbstractEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;
    @Column(updatable = false)
    private LocalDateTime dateRegister;
    private LocalDateTime dateUpdate;
    @Version
    @Column(columnDefinition = "integer DEFAULT 0", nullable = false)
    private Long version;

    /** controller, gets e sets omitidos**/
}
