var directives = angular.module('pzDirectives', []);
directives.directive('pzTitle', function () {
    return{
        restrict: 'E',
        transclude: true,
        template: '<h1 class="col-lg-12" ng-transclude></h1>'
    };
});