var pizzaria = angular.module('pizzariaAPP');
pizzaria.controller('StatesController', function ($scope, $http) {
    $scope.states = [];
    $http.get('http://localhost:8080/pizzaria/state')
            .success(function (states) {
                $scope.states = states;
            })
            .error(function (erro) {
                console.log(erro);
            });
});