@EnableTransactionManagement
public class JPAConfig {
	@Bean
	public LocalContainerEntityManagerFactoryBean getEntityManagerFactory
		(DataSource dataSource) {
		LocalContainerEntityManagerFactoryBean entityManagerFactory
			= new LocalContainerEntityManagerFactoryBean();
		entityManagerFactory.setPackagesToScan(packageName(State.class));
		entityManagerFactory.setDataSource(dataSource);
		entityManagerFactory.setJpaVendorAdapter(
			new HibernateJpaVendorAdapter());
		entityManagerFactory.setJpaProperties(getProperties());
		return entityManagerFactory;
	}
	private Properties getProperties() {
		Properties props = new Properties();
		props.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
		props.setProperty("hibernate.show_sql", "true");
		props.setProperty("hibernate.hbm2ddl.auto", "create-drop");
		return props;
	}
	@Bean
	public DataSource getDataSource() {
		DriverManagerDataSource dataSource = new
			DriverManagerDataSource();
		dataSource.setDriverClassName("org.postgresql.Driver");
		dataSource.setUrl("jdbc:postgresql://localhost/pizzaria");
		dataSource.setUsername("postgres");
		dataSource.setPassword("Postgres1234");
		return dataSource;
	}
	@Bean
	public JpaTransactionManager getTransactionManager
		(EntityManagerFactory emf) {
		return new JpaTransactionManager(emf);
	}
	private String packageName(Class<?> clazz) {
		return clazz.getPackage().getName();
	}
}