var services = angular.module('pzServices', ['ngResource']);
services.factory('stateResources', function ($resource) {
    return $resource('http://localhost:8080/pizzaria/state/:stateId', null, {
        'update': {
            method: 'PUT'
        }
    });
});