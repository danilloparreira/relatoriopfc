public abstract interface DAOGeneric<T extends AbstractEntity> {
	public T find(Long id);
	public List<T> findAll();
	public T save(T entity);
	public T merge(T entity);
	public void delete(T entity);
}
