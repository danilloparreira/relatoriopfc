var pizzariaAPP = angular.module('pizzariaAPP',
    ['ngRoute','StateServices','pzDirectives']);
pizzariaAPP.config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/state', {
            templateUrl: 'partials/state/list.html',
            controller: 'StatesController'
        }).when('/state/new', {
            templateUrl: 'partials/state/form.html',
            controller: 'StateController'
        }).when('/state/edit/:id', {
            templateUrl: 'partials/state/form.html',
            controller: 'StateController'
        });
        
        $routeProvider.when('/register', {
            templateUrl: 'partials/global/register.html'
        }).when('/index', {
            templateUrl: 'partials/global/home.html'
        }).otherwise({redirectTo: '/index'});
    }]);