@Configuration
@EnableWebMvc
@ComponentScan(basePackageClasses = { StateController.class, StateDAO.class })
public class WebConfig extends WebMvcConfigurerAdapter {
}
