public abstract class DAOGenericImpl<T extends AbstractEntity>
	implements DAOGeneric<T> {

	@PersistenceContext
	protected EntityManager manager;
	private Class<? extends T> clazz;

	public DAOGenericImpl() {
		ParameterizedType genericSuperclass =
			(ParameterizedType) getClass().
			getGenericSuperclass();
		this.clazz = (Class<T>) genericSuperclass.
			getActualTypeArguments()[0];
	}
	@Override
	public T find(Long id) {
		return manager.find(clazz, id);
	}
	/** Outros m�todos omitidos **/

}
