
public abstract class RESTControllerImpl <T extends AbstractEntity,
					DAO extends DAOGeneric<T>>
		implements RESTController<T, DAO> {

	@Autowired
	private DAO dao;

	@Override
	@RequestMapping(value = "/{id}", method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<T> find(@PathVariable("id") Long id) {
		T entity = (T) dao.find(id);
		if (entity == null) {
			return new ResponseEntity<>
				(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(entity, HttpStatus.OK);
	}
	/* Outros m�todos omitidos */
}